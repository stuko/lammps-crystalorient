# LAMMPS package for crystal orientation calculation

> **NOTE: This repository is for internal use only! Do not use the code from this repository without approval from the author.**

This repository hosts the source code of a [LAMMPS](http://lammps.sandia.gov) extension package that can compute the lattice  
orientation of a single crystal in molecular dynamics simulations. Currently, the package supports FCC and BCC crystals.

## Installation

To install the package, clone this Git repository into a subdirectory named ``USER-CRYSTALORIENT`` under the ``src/`` directory 
of your local LAMMPS installation:
```
cd <lammps-path>/src/
git clone https://gitlab.com/stuko/lammps-crystalorient.git USER-CRYSTALORIENT
```
Then activate the extension package with the command
```
make yes-user-crystalorient
``` 
Finally, rebuild LAMMPS by running [``make <machine-name>``](http://lammps.sandia.gov/doc/Section_start.html#making-lammps).

## Usage

```
compute ID group-ID crystalorient lattice 
```

* ``ID`` = user-assigned name for the compute
* ``group-ID`` = ID of the group of atoms to apply the compute to (typically the "all" group)
* ``lattice`` = the crystal structure type, either ``fcc`` or ``bcc``

The compute performs an adaptive common neighbor analysis to identify crystalline atoms, groups them into contiguous clusters, finds the largest cluster in the system, and computes the mean elastic deformation gradient F<sup>e</sup> for that crystallite. Finally, the polar decomposition F<sup>e</sup>=R*U is computed to obtain the orientation matrix R describing the lattice rotation with respect to the simulation coordinate system. The computed orientation is mapped to the fundamental zone of the cubic crystal lattice.

### Output

The LAMMPS compute outputs a global vector of length 10. The first 9 elements contain the 3x3 orientation matrix. The 10th element contains the fraction of atoms that are currently part of the largest crystallite cluster in the system for which the orientation has been computed.  

### Usage example

```
compute       orient all crystalorient bcc
thermo_style  custom step c_orient[1] c_orient[2] c_orient[3] c_orient[4] c_orient[5] c_orient[6] c_orient[7] c_orient[8] c_orient[9] c_orient[10]
```

## Algorithm synopsis

1. Use adaptive CNA to identify all perfect crystal atoms of the selected lattice type.
2. Compute the local crystal orientation for each perfect crystal atom. To this end, 
   * Generate a correspondence mapping of its nearest neighbors to the ideal reference lattice. 
   * Use a least-squares fit to compute a per-atom elastic deformation gradient, which includes the orientation information and elastic lattice strains.
3. Use a parallelized clustering algorithm to group the crystal atoms into disconnected sets. Each set forms a crystallite containing only atoms with similar orientations.
4. Compute a mean elastic deformation gradient for each crystal cluster. Here, make sure that cubic crystal symmetry is accounted for correctly. 
5. Determine which cluster contains the largest number of atoms.
6. Compute polar decomposition of elastic deformation gradient to obtain orientation matrix. Map orientation to fundamental zone. 

## Author and contact

* [Alexander Stukowski](http://www.mawi.tu-darmstadt.de/mm/mm_mm_sw/gruppe_mm_sw_1/mitarbeiterdetails_mm_2307.en.jsp),
  Technische Universität Darmstadt, Germany
