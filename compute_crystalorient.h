/* ----------------------------------------------------------------------
 * LAMMPS compute that calculates the current lattice orientation of a 
 * single crystal.
 *
 * Author: Alexander Stukowski (stukowski@mm.tu-darmstadt.de)
------------------------------------------------------------------------- */

#ifdef COMPUTE_CLASS

ComputeStyle(crystalorient, CrystalOrientationCompute)

#else

#ifndef __COMP_CRYSTALORIENT_LAMMPS_H
#define __COMP_CRYSTALORIENT_LAMMPS_H

#include "compute.h"
#include "memory.h"
#include <string>
#include <vector>
#include <math.h>
#include <cstring>

using namespace LAMMPS_NS;

class CrystalOrientationCompute : public Compute
{
public:

    /// 3d vector data structure used throughout this code.
    class Vector3 
    {
    public:
        Vector3() {}
        Vector3(double x, double y, double z) { _data[0] = x; _data[1]= y; _data[2] = z; }
        double x() const { return _data[0]; }
        double y() const { return _data[1]; }
        double z() const { return _data[2]; }
        double operator[](size_t index) const { return _data[index]; }
        double& operator[](size_t index) { return _data[index]; }
        double squared_length() const { return x()*x() + y()*y() + z()*z(); }
        double length() const { return sqrt(squared_length()); }
        Vector3 operator-(const Vector3& v) const { return Vector3( x() - v.x(), y() - v.y(), z() - v.z() ); }
        Vector3 operator+(const Vector3& v) const { return Vector3( x() + v.x(), y() + v.y(), z() + v.z() ); }
        Vector3& operator+=(const Vector3& v) { _data[0] += v._data[0]; _data[1] += v._data[1]; _data[2] += v._data[2]; return *this; }
        Vector3& operator-=(const Vector3& v) { _data[0] -= v._data[0]; _data[1] -= v._data[1]; _data[2] -= v._data[2]; return *this; }
        Vector3 operator-() const { return Vector3(-x(), -y(), -z()); }
    private:
        double _data[3];
    };

    /// 3d matrix data structure used throughout this code.
    class Matrix3 
    {
    public:
        Matrix3() {
            setZero();
        }
        const Vector3& operator[](size_t index) const { return _data[index]; }
        Vector3& operator[](size_t index) { return _data[index]; }
        double determinant() const {
            return (*this)[0][0] * ((*this)[1][1] * (*this)[2][2] - (*this)[2][1] * (*this)[1][2]) -
                (*this)[0][1] * ((*this)[1][0] * (*this)[2][2] - (*this)[1][2] * (*this)[2][0]) +
                (*this)[0][2] * ((*this)[1][0] * (*this)[2][1] - (*this)[1][1] * (*this)[2][0]);
        }
        double trace() const {
            return (*this)[0][0] + (*this)[1][1] + (*this)[2][2];
        }
        void setZero() { memset(_data, 0, sizeof(_data)); }
    private:
        Vector3 _data[3];
    };

    /// The information associated with each crystallite cluster. 
    struct ClusterInfo {
        double atomCount;
        Matrix3 orient;
    };

public:

    /// Constructor.
    CrystalOrientationCompute(class LAMMPS*, int, char **);

    /// Destructor.
    ~CrystalOrientationCompute();

    /// This gets called by the system before the simulation starts.
    virtual void init();

    /// Called by LAMMPS when compute is evaluated.
    void compute_vector();

    /// Reports the memory usage of this fix to LAMMPS.
    virtual double memory_usage();

    /// Neighbor list initialization.
    virtual void init_list(int id, NeighList *ptr) {
        list = ptr;
    }

    /// This is for MPI communication with neighbor nodes.
    virtual int pack_forward_comm(int n, int *list, double *buf, int pbc_flag, int *pbc);

    /// This is for MPI communication with neighbor nodes.
    virtual void unpack_forward_comm(int n, int first, double *buf);

private:

    /// Sends a formatted string to the log file and stdout.
    void printLog(const char* format, ...) const;

    // Callback function for ring communication.
    static void idring(int, char *, void *);

    /// Computes the mean of two crystal orientations. 
    static void combine_orientations(double& atomCountA, Matrix3& orientA, const double atomCountB, const Matrix3& orientB);

    static int polar_decomposition_3x3(double* _A, bool right_sided, double* U, double* P);

    static void map_to_fundamental_zone(Matrix3& orientation);

public:

    /*
    A bit-flag array that stores which pairs of neighbors are bonded
    and which are not (using bit-wise storage).
    */
    struct NeighborBondArray
    {
        /// Maximum number of neighbors in coordination structures recognized by the CNA.
        enum { CNA_MAX_PATTERN_NEIGHBORS = 14 };

        /// Two-dimensional bit array that stores the bonds between neighbors.
        unsigned int neighborArray[CNA_MAX_PATTERN_NEIGHBORS];

        /// Default constructor.
        NeighborBondArray() { memset(neighborArray, 0, sizeof(neighborArray)); }

        /// Returns whether two nearest neighbors have a bond between them.
        bool neighborBond(int neighborIndex1, int neighborIndex2) const {
            return (neighborArray[neighborIndex1] & (1<<neighborIndex2));
        }

        /// Sets whether two nearest neighbors have a bond between them.
        void setNeighborBond(int neighborIndex1, int neighborIndex2, bool bonded) {
            if(bonded) {
                neighborArray[neighborIndex1] |= (1<<neighborIndex2);
                neighborArray[neighborIndex2] |= (1<<neighborIndex1);
            }
            else {
                neighborArray[neighborIndex1] &= ~(1<<neighborIndex2);
                neighborArray[neighborIndex2] &= ~(1<<neighborIndex1);
            }
        }
    };

protected:

    enum { CLUSTER };

    /// The name of the input lattice structure, e.g. "bcc".
    std::string latticeStructure;

    /// The ideal lattice vectors of the structure template.
    std::vector<Vector3> latticeVectors;

    /// The CNA bond signatures of the reference structure template. 
    std::vector<int> cnaSignaturesReference;

    /// The connectivity graph for the reference structure.
    NeighborBondArray neighborArrayReference;

    /// LAMMPS neighbor list.
    class NeighList *list;

    /// Determines the information to be transfered between procs.
    int commflag;

    /// Enables debugging print statements.
    bool debugOutput;

    /// Array holding the results of the atomic structure identification phase.
    std::vector<ClusterInfo> local_orientations;

    /// Symmetry transformations of the lattice.
    static std::vector<Matrix3> symmetryElements;

    MPI_Datatype cluster_info_data_type;
    MPI_Op orientation_reduction_op;
    static void mpi_combine_orientations(void *invec, void *inoutvec, int *len, MPI_Datatype *datatype);
};

#endif // __COMP_CRYSTALORIENT_LAMMPS_H

#endif // COMPUTE_CLASS
