/* ----------------------------------------------------------------------
 * LAMMPS compute that calculates the current lattice orientation of the
 * largest crystallite in the system.
 *
 * Author: Alexander Stukowski (stukowski@mm.tu-darmstadt.de)
------------------------------------------------------------------------- */

#include "compute_crystalorient.h"
#include "force.h"
#include "comm.h"
#include "modify.h"
#include "domain.h"
#include "error.h"
#include "atom.h"
#include "neighbor.h"
#include "neigh_list.h"
#include "neigh_request.h"
#include "error.h"
#include "update.h"
#include "group.h"

#include <fstream>
#include <sstream>
#include <cstdio>
#include <algorithm>
#include <set>
#include <cstdarg>
#include <cassert>
#include <limits>

#include <sys/stat.h>
#include <errno.h>

/// This is to identify the code version in use in the log and output data files.
#define COMPUTE_CRYSTALORIENT_CODE_VERSION      "1.3.6"

// Symmetry transformations of the lattice.
std::vector<CrystalOrientationCompute::Matrix3> CrystalOrientationCompute::symmetryElements;

/******************************************************************************
* Constructor.
******************************************************************************/
CrystalOrientationCompute::CrystalOrientationCompute(LAMMPS *lmp, int narg, char **arg) :
    Compute(lmp, narg, arg)
{
    debugOutput = false;
    vector_flag = 1;
    size_vector = 10;
    extvector = 0;

    comm_forward = 1;
    vector = new double[size_vector];

    // Parse compute command parameters.
    int iarg = 3;
    if(narg < 4) error->all(FLERR, "Not enough parameters specified for compute crystalorient.");
    if(narg > 4) error->all(FLERR, "Too many parameters specified for compute crystalorient.");

    // Select lattice type and initialize ideal lattice vectors.
    latticeStructure = arg[iarg++];
    if(latticeStructure == "bcc") {
        // 1/2<111> nearest neighbor vectors in standard Bravais cell:
        latticeVectors.push_back(Vector3(0.5,0.5,0.5)); cnaSignaturesReference.push_back(0);
        latticeVectors.push_back(Vector3(-0.5,0.5,0.5)); cnaSignaturesReference.push_back(0);
        latticeVectors.push_back(Vector3(0.5,-0.5,0.5)); cnaSignaturesReference.push_back(0);
        latticeVectors.push_back(Vector3(-0.5,-0.5,0.5)); cnaSignaturesReference.push_back(0);
        latticeVectors.push_back(Vector3(0.5,0.5,-0.5)); cnaSignaturesReference.push_back(0);
        latticeVectors.push_back(Vector3(-0.5,0.5,-0.5)); cnaSignaturesReference.push_back(0);
        latticeVectors.push_back(Vector3(0.5,-0.5,-0.5)); cnaSignaturesReference.push_back(0);
        latticeVectors.push_back(Vector3(-0.5,-0.5,-0.5)); cnaSignaturesReference.push_back(0);
        // <100> second nearest neighbor vectors in standard Bravais cell:
        latticeVectors.push_back(Vector3(1.0,0.0,0.0)); cnaSignaturesReference.push_back(1);
        latticeVectors.push_back(Vector3(-1.0,0.0,0.0)); cnaSignaturesReference.push_back(1);
        latticeVectors.push_back(Vector3(0.0,1.0,0.0)); cnaSignaturesReference.push_back(1);
        latticeVectors.push_back(Vector3(0.0,-1.0,0.0)); cnaSignaturesReference.push_back(1);
        latticeVectors.push_back(Vector3(0.0,0.0,1.0)); cnaSignaturesReference.push_back(1);
        latticeVectors.push_back(Vector3(0.0,0.0,-1.0)); cnaSignaturesReference.push_back(1);
        for(int ni1 = 0; ni1 < 14; ni1++) {
            neighborArrayReference.setNeighborBond(ni1, ni1, false);
            for(int ni2 = ni1 + 1; ni2 < 14; ni2++) {
                bool bonded = (latticeVectors[ni1] - latticeVectors[ni2]).length() < (1.0+sqrt(2.0))*0.5;
                neighborArrayReference.setNeighborBond(ni1, ni2, bonded);
            }
        }       
    }
    else if(latticeStructure == "fcc") {
        // 1/2<110> nearest neighbor vectors in standard Bravais cell:
        latticeVectors.push_back(Vector3( 0.5,  0.5,  0.0)); cnaSignaturesReference.push_back(0);
        latticeVectors.push_back(Vector3( 0.0,  0.5,  0.5)); cnaSignaturesReference.push_back(0);
        latticeVectors.push_back(Vector3( 0.5,  0.0,  0.5)); cnaSignaturesReference.push_back(0);
        latticeVectors.push_back(Vector3(-0.5, -0.5,  0.0)); cnaSignaturesReference.push_back(0);
        latticeVectors.push_back(Vector3( 0.0, -0.5, -0.5)); cnaSignaturesReference.push_back(0);
        latticeVectors.push_back(Vector3(-0.5,  0.0, -0.5)); cnaSignaturesReference.push_back(0);
        latticeVectors.push_back(Vector3(-0.5,  0.5,  0.0)); cnaSignaturesReference.push_back(0);
        latticeVectors.push_back(Vector3( 0.0, -0.5,  0.5)); cnaSignaturesReference.push_back(0);
        latticeVectors.push_back(Vector3(-0.5,  0.0,  0.5)); cnaSignaturesReference.push_back(0);
        latticeVectors.push_back(Vector3( 0.5, -0.5,  0.0)); cnaSignaturesReference.push_back(0);
        latticeVectors.push_back(Vector3( 0.0,  0.5, -0.5)); cnaSignaturesReference.push_back(0);
        latticeVectors.push_back(Vector3( 0.5,  0.0, -0.5)); cnaSignaturesReference.push_back(0);
        for(int ni1 = 0; ni1 < 12; ni1++) {
            neighborArrayReference.setNeighborBond(ni1, ni1, false);
            for(int ni2 = ni1 + 1; ni2 < 12; ni2++) {
                bool bonded = (latticeVectors[ni1] - latticeVectors[ni2]).length() < (1.0+sqrt(0.5))*0.5;
                neighborArrayReference.setNeighborBond(ni1, ni2, bonded);
            }
        }       
    }
    else error->all(FLERR,"Invalid lattice type in compute crystalorient command.");

    // Precompute list of space group symmetry operations.
    // For the cubic lattice, there will be 24 rotations. 
    if(symmetryElements.empty()) {
        for(int s1 = -1; s1 <= 1; s1 += 2) {
            for(int s2 = -1; s2 <= 1; s2 += 2) {
                for(int s3 = -1; s3 <= 1; s3 += 2) {
                    int p[3] = {0,1,2};
                    do {
                        Matrix3 m;
                        m[p[0]][0] = s1;
                        m[p[0]][1] = 0;
                        m[p[0]][2] = 0;
                        m[p[1]][0] = 0;
                        m[p[1]][1] = s2;
                        m[p[1]][2] = 0;
                        m[p[2]][0] = 0;
                        m[p[2]][1] = 0;
                        m[p[2]][2] = s3;
                        if(m.determinant() > 0) // Filter out reflection transformations.
                            symmetryElements.push_back(m);
                    }
                    while(std::next_permutation(p, p+3));
                }
            }
        }
    }
    
    // Create the MPI data type for transmitting crystallite cluster atom counts and orientation matrices. 
    MPI_Type_contiguous(10, MPI_DOUBLE, &cluster_info_data_type);
    MPI_Type_commit(&cluster_info_data_type);

    // Register the MPI reduction operation for combining orientations of crystallite clusters. 
    MPI_Op_create(&mpi_combine_orientations, true, &orientation_reduction_op);
}

/*********************************************************************
 * Destructor.
 *********************************************************************/
CrystalOrientationCompute::~CrystalOrientationCompute()
{
    delete[] vector;
    MPI_Op_free(&orientation_reduction_op);
    MPI_Type_free(&cluster_info_data_type);
}

/*********************************************************************
 * This gets called by the system before the simulation starts.
 *********************************************************************/
void CrystalOrientationCompute::init()
{
    printLog("Code version: %s\n", COMPUTE_CRYSTALORIENT_CODE_VERSION);
    printLog("Crystal lattice type: %s\n", latticeStructure.c_str());

    // Need an occasional full neighbor list
    int irequest = neighbor->request(this,instance_me);
    neighbor->requests[irequest]->pair = 0;
    neighbor->requests[irequest]->compute = 1;
    neighbor->requests[irequest]->half = 0;
    neighbor->requests[irequest]->full = 1;
    neighbor->requests[irequest]->occasional = 1;
}

// Pair of neighbor atoms that form a bond (bit-wise storage, i.e., two bits are set).
typedef unsigned int CNAPairBond;

/*
 A nearest neighbor of the central atom.
 Stores vector and squared distance.
 */
struct AdaptiveCNANeighbor {
    CrystalOrientationCompute::Vector3 del;
    double distSq;
    int atomIndex;

    // Used for sorting of neighbor list.
    bool operator<(const AdaptiveCNANeighbor& o) const { return distSq < o.distSq; }
    bool operator>(const AdaptiveCNANeighbor& o) const { return distSq > o.distSq; }
};

static inline int findCommonNeighbors(const CrystalOrientationCompute::NeighborBondArray& neighborArray, int neighborIndex, unsigned int& commonNeighbors, int numNeighbors);
static inline int findNeighborBonds(const CrystalOrientationCompute::NeighborBondArray& neighborArray, unsigned int commonNeighbors, int numNeighbors, CNAPairBond* neighborBonds);
static inline int calcMaxChainLength(CNAPairBond* neighborBonds, int numBonds);

/// Fast sorting function for an array of (bounded) integers.
/// Sorts values in descending order.
template<typename iterator>
void bitmapSort(iterator begin, iterator end, int max)
{
    int bitarray = 0;
    for(iterator pin = begin; pin != end; ++pin) {
        bitarray |= 1 << (*pin);
    }
    iterator pout = begin;
    for(int i = max - 1; i >= 0; i--)
        if(bitarray & (1 << i))
            *pout++ = i;
}

static void matrix_inverse(const CrystalOrientationCompute::Matrix3& m, CrystalOrientationCompute::Matrix3& minv) 
{
    // computes the inverse of a matrix m
    double invdet = 1.0 / m.determinant();

    minv[0][0] = (m[1][1] * m[2][2] - m[2][1] * m[1][2]) * invdet;
    minv[0][1] = (m[0][2] * m[2][1] - m[0][1] * m[2][2]) * invdet;
    minv[0][2] = (m[0][1] * m[1][2] - m[0][2] * m[1][1]) * invdet;
    minv[1][0] = (m[1][2] * m[2][0] - m[1][0] * m[2][2]) * invdet;
    minv[1][1] = (m[0][0] * m[2][2] - m[0][2] * m[2][0]) * invdet;
    minv[1][2] = (m[1][0] * m[0][2] - m[0][0] * m[1][2]) * invdet;
    minv[2][0] = (m[1][0] * m[2][1] - m[2][0] * m[1][1]) * invdet;
    minv[2][1] = (m[2][0] * m[0][1] - m[0][0] * m[2][1]) * invdet;
    minv[2][2] = (m[0][0] * m[1][1] - m[1][0] * m[0][1]) * invdet;
}

static void matrix_multiply(const CrystalOrientationCompute::Matrix3& a, const CrystalOrientationCompute::Matrix3& b, CrystalOrientationCompute::Matrix3& out) 
{
    out[0][0] = a[0][0]*b[0][0] + a[0][1]*b[1][0] + a[0][2]*b[2][0];
    out[0][1] = a[0][0]*b[0][1] + a[0][1]*b[1][1] + a[0][2]*b[2][1];
    out[0][2] = a[0][0]*b[0][2] + a[0][1]*b[1][2] + a[0][2]*b[2][2];

    out[1][0] = a[1][0]*b[0][0] + a[1][1]*b[1][0] + a[1][2]*b[2][0];
    out[1][1] = a[1][0]*b[0][1] + a[1][1]*b[1][1] + a[1][2]*b[2][1];
    out[1][2] = a[1][0]*b[0][2] + a[1][1]*b[1][2] + a[1][2]*b[2][2];

    out[2][0] = a[2][0]*b[0][0] + a[2][1]*b[1][0] + a[2][2]*b[2][0];
    out[2][1] = a[2][0]*b[0][1] + a[2][1]*b[1][1] + a[2][2]*b[2][1];
    out[2][2] = a[2][0]*b[0][2] + a[2][1]*b[1][2] + a[2][2]*b[2][2];    
}

/*********************************************************************
 * Called by LAMMPS when compute is evaluated.
 *********************************************************************/
void CrystalOrientationCompute::compute_vector()
{
    invoked_vector = update->ntimestep;
    if(debugOutput)
        printLog("Calculating crystal orientation on timestep %i\n", update->ntimestep);

    int inum;
    int *ilist,*numneigh,**firstneigh;  

    // Grow array if necessary
    if(atom->nmax > local_orientations.size()) {
        local_orientations.resize(atom->nmax);
    }

    // Invoke full neighbor list (will copy or build if necessary)
    neighbor->build_one(list);

    inum = list->inum;
    ilist = list->ilist;
    numneigh = list->numneigh;
    firstneigh = list->firstneigh;

    double **x = atom->x;
    int *mask = atom->mask;
    tagint *tag = atom->tag;

    // Perform adaptive CNA.
    std::vector<AdaptiveCNANeighbor> neighborVectors;
    int maxneigh = 0;
    int numcrystalline = 0;
    for(int ii = 0; ii < inum; ii++) {
        int i = ilist[ii];

        local_orientations[i].atomCount = 0;
        local_orientations[i].orient.setZero();
            
        if(!(mask[i] & groupbit)) {
            continue;
        }

        double xtmp = x[i][0];
        double ytmp = x[i][1];
        double ztmp = x[i][2];
        int* jlist = firstneigh[i];
        int jnum = numneigh[i];

        // Number of neighbors to analyze.
        int nn = latticeVectors.size();

        // Early rejection of under-coordinated atoms:
        if(jnum < nn)
            continue;

        if(jnum > neighborVectors.size())
            neighborVectors.resize(jnum);

        // Generate full list of bond vectors.
        for (int jj = 0; jj < jnum; jj++) {
            int j = jlist[jj];
            j &= NEIGHMASK;

            AdaptiveCNANeighbor& n = neighborVectors[jj];
            n.del[0] = xtmp - x[j][0];
            n.del[1] = ytmp - x[j][1];
            n.del[2] = ztmp - x[j][2];
            n.distSq = n.del.squared_length();
        }

        // Select N nearest neighbors from full list and sort them.
        std::partial_sort(neighborVectors.begin(), neighborVectors.begin() + nn, neighborVectors.begin() + jnum);

        // Compute scaling factor.
        double localScaling = 0;
        if(nn == 14) {
            // BCC:
            for(int n = 0; n < 8; n++)
                localScaling += sqrt(neighborVectors[n].distSq) / 0.866025404;
            for(int n = 8; n < 14; n++)
                localScaling += sqrt(neighborVectors[n].distSq);
        }
        else {
            // FCC:
            for(int n = 0; n < 12; n++)
                localScaling += sqrt(neighborVectors[n].distSq);
        }
        localScaling /= nn;
        double localCutoffSquared = localScaling * localScaling * (1.207106781 * 1.207106781);

        // Compute bond bit-flag array.
        NeighborBondArray neighborArray;
        for(int ni1 = 0; ni1 < nn; ni1++) {
            neighborArray.setNeighborBond(ni1, ni1, false);
            for(int ni2 = ni1+1; ni2 < nn; ni2++) {
                double delx = neighborVectors[ni1].del[0] - neighborVectors[ni2].del[0];
                double dely = neighborVectors[ni1].del[1] - neighborVectors[ni2].del[1];
                double delz = neighborVectors[ni1].del[2] - neighborVectors[ni2].del[2];
                double d = delx*delx + dely*dely + delz*delz;
                neighborArray.setNeighborBond(ni1, ni2, d <= localCutoffSquared);
            }
        }

        int cnaSignatures[NeighborBondArray::CNA_MAX_PATTERN_NEIGHBORS];
        if(nn == 14) {
            // BCC:
            int n444 = 0;
            int n666 = 0;
            for(int ni = 0; ni < nn; ni++) {

                // Determine number of neighbors the two atoms have in common.
                unsigned int commonNeighbors;
                int numCommonNeighbors = findCommonNeighbors(neighborArray, ni, commonNeighbors, nn);
                if(numCommonNeighbors != 4 && numCommonNeighbors != 6)
                    break;

                // Determine the number of bonds among the common neighbors.
                CNAPairBond neighborBonds[NeighborBondArray::CNA_MAX_PATTERN_NEIGHBORS*NeighborBondArray::CNA_MAX_PATTERN_NEIGHBORS];
                int numNeighborBonds = findNeighborBonds(neighborArray, commonNeighbors, nn, neighborBonds);
                if(numNeighborBonds != 4 && numNeighborBonds != 6)
                    break;

                // Determine the number of bonds in the longest continuous chain.
                int maxChainLength = calcMaxChainLength(neighborBonds, numNeighborBonds);
                if(numCommonNeighbors == 4 && numNeighborBonds == 4 && maxChainLength == 4) {
                    n444++;
                    cnaSignatures[ni] = 1;
                }
                else if(numCommonNeighbors == 6 && numNeighborBonds == 6 && maxChainLength == 6) {
                    n666++;
                    cnaSignatures[ni] = 0;
                }
                else break;
            }
            if(n666 != 8 || n444 != 6)
                continue;
        }
        else {
            // FCC:
            int n421 = 0;
            int n422 = 0;
            for(int ni = 0; ni < nn; ni++) {

                // Determine number of neighbors the two atoms have in common.
                unsigned int commonNeighbors;
                int numCommonNeighbors = findCommonNeighbors(neighborArray, ni, commonNeighbors, nn);
                if(numCommonNeighbors != 4)
                    break;

                // Determine the number of bonds among the common neighbors.
                CNAPairBond neighborBonds[NeighborBondArray::CNA_MAX_PATTERN_NEIGHBORS*NeighborBondArray::CNA_MAX_PATTERN_NEIGHBORS];
                int numNeighborBonds = findNeighborBonds(neighborArray, commonNeighbors, nn, neighborBonds);
                if(numNeighborBonds != 2)
                    break;

                // Determine the number of bonds in the longest continuous chain.
                int maxChainLength = calcMaxChainLength(neighborBonds, numNeighborBonds);
                if(maxChainLength == 1) {
                    n421++;
                    cnaSignatures[ni] = 0;
                }
                else break;
            }
            if(n421 != 12)
                continue;
        }

        // Initialize permutation.
        int neighborMapping[NeighborBondArray::CNA_MAX_PATTERN_NEIGHBORS];
        int previousMapping[NeighborBondArray::CNA_MAX_PATTERN_NEIGHBORS];
        for(int n = 0; n < nn; n++) {
            neighborMapping[n] = n;
            previousMapping[n] = -1;
        }

        // Find first matching neighbor permutation.
        for(;;) {
            int ni1 = 0;
            while(neighborMapping[ni1] == previousMapping[ni1]) {
                ni1++;
            }
            for(; ni1 < nn; ni1++) {
                int atomNeighborIndex1 = neighborMapping[ni1];
                previousMapping[ni1] = atomNeighborIndex1;
                if(cnaSignatures[atomNeighborIndex1] != cnaSignaturesReference[ni1]) {
                    break;
                }
                int ni2;
                for(ni2 = 0; ni2 < ni1; ni2++) {
                    int atomNeighborIndex2 = neighborMapping[ni2];
                    if(neighborArray.neighborBond(atomNeighborIndex1, atomNeighborIndex2) != neighborArrayReference.neighborBond(ni1, ni2)) {
                        break;
                    }
                }
                if(ni2 != ni1) break;
            }
            if(ni1 == nn) {
                // Assign coordination structure type to atom.
                local_orientations[i].atomCount = tag[i];
                numcrystalline++;

                // For calculating the elastic deformation gradient.
                Matrix3 orientationV;
                Matrix3 orientationW;

                for(int n = 0; n < nn; n++) {
                    int neighborAtomIndex = neighborMapping[n];
                    // Add vector pair to matrices for computing the elastic deformation gradient.
                    const Vector3& latticeVector = latticeVectors[n];
                    const Vector3& spatialVector = neighborVectors[neighborAtomIndex].del;
                    for(int i = 0; i < 3; i++) {
                        for(int j = 0; j < 3; j++) {
                            orientationV[i][j] += (latticeVector[j] * latticeVector[i]);
                            orientationW[i][j] += (latticeVector[j] * spatialVector[i]);
                        }
                    }
                }

                // Calculate deformation gradient tensor.
                Matrix3 orientationVinverse;
                Matrix3& F = local_orientations[i].orient;
                matrix_inverse(orientationV, orientationVinverse);
                matrix_multiply(orientationW, orientationVinverse, F);
                break;
            }
            bitmapSort(neighborMapping + ni1 + 1, neighborMapping + nn, nn);
            if(!std::next_permutation(neighborMapping, neighborMapping + nn)) {
                // This should not happen.
                assert(false);
                break;
            }
        }
    }

    if(debugOutput) {
        bigint ncryst_local = numcrystalline;
        bigint ncryst_all;
        MPI_Allreduce(&ncryst_local, &ncryst_all, 1, MPI_LMP_BIGINT, MPI_SUM, world);
        printLog("Total number of crystalline atoms: %i\n", ncryst_all);
    }

    // Clustering algorithm.
    commflag = CLUSTER;
    for(int iter = 1; ; iter++) {
        if(debugOutput) {
            MPI_Barrier(world);
            printLog("Clustering iteration: %i\n", iter);
        }
        comm->forward_comm_compute(this);

        int change = 0;
        for(;;) {
            bool done = true;
            for(int ii = 0; ii < inum; ii++) {
                int i = ilist[ii];
                if(!(mask[i] & groupbit)) continue;
                double& cluster_i = local_orientations[i].atomCount;
                if(cluster_i == 0) continue;

                double xtmp = x[i][0];
                double ytmp = x[i][1];
                double ztmp = x[i][2];
                int* jlist = firstneigh[i];
                int jnum = numneigh[i];

                if(jnum > neighborVectors.size())
                    error->one(FLERR,"Invalid  neighbor array size. This is a bug.");

                // Generate full list of bond vectors.
                for(int jj = 0; jj < jnum; jj++) {
                    int j = jlist[jj];
                    j &= NEIGHMASK;

                    AdaptiveCNANeighbor& n = neighborVectors[jj];
                    n.del[0] = xtmp - x[j][0];
                    n.del[1] = ytmp - x[j][1];
                    n.del[2] = ztmp - x[j][2];
                    n.distSq = n.del.squared_length();
                    n.atomIndex = j;
                }

                int nn = latticeVectors.size();
                if(nn > jnum)
                    error->one(FLERR,"Invalid number of neighbors. This is a bug.");

                // Select N nearest neighbors from full list and sort them.
                std::partial_sort(neighborVectors.begin(), neighborVectors.begin() + nn, neighborVectors.begin() + jnum);

                // Clustering merging algorithm.
                for(int j = 0; j < nn; j++) {
                    double& cluster_j = local_orientations[neighborVectors[j].atomIndex].atomCount;
                    if(cluster_j != cluster_i && cluster_j != 0) {
                        cluster_i = cluster_j = MIN(cluster_i,cluster_j);
                        done = false;
                    }
                }
            }
            if(!done) change = 1;
            else break;
        }
        // Stop if all procs are done.
        int anychange;
        MPI_Allreduce(&change, &anychange, 1, MPI_INT, MPI_MAX, world);
        if(!anychange) break;
    }
    if(debugOutput) {
        MPI_Barrier(world);
        printLog("Clustering algorithm complete\n");
    }

    // Compress cluster IDs via map of the original uncompressed IDs.
    std::map<tagint,int> idmap;
    for(int i = 0; i < atom->nlocal; i++) {
        tagint id = (tagint)local_orientations[i].atomCount;
        if(id == 0) continue;
        idmap.insert(std::make_pair(id, 0));
    }
    bigint nbone = idmap.size();
    bigint nball;
    MPI_Allreduce(&nbone, &nball, 1, MPI_LMP_BIGINT, MPI_SUM, world);
    if(debugOutput) {
        MPI_Barrier(world);
        printLog("Aggregate number of clusters (nball): %i\n", nball);
    }

    // Create my list of populated IDs
    std::vector<tagint> list(idmap.size());

    std::map<tagint,int>::iterator pos;
    int n = 0;
    for(pos = idmap.begin(); pos != idmap.end(); ++pos)
        list[n++] = pos->first;

    // If nall < 1M, just allgather all ID lists on every proc
    // else perform ring comm. Add IDs from all procs to my map.
    if(nball <= 1024*1024) {

        // Setup for allgatherv
    
        int nprocs = comm->nprocs;
        int nall = nball;
        std::vector<tagint> listall(nall);
        std::vector<int> recvcounts(nprocs);
        std::vector<int> displs(nprocs);

        MPI_Allgather(&n, 1, MPI_INT, recvcounts.data(), 1, MPI_INT, world);

        displs[0] = 0;
        for(int iproc = 1; iproc < nprocs; iproc++)
            displs[iproc] = displs[iproc-1] + recvcounts[iproc-1];

        // allgatherv acquires list of populated IDs from all procs
        MPI_Allgatherv(list.data(), n, MPI_LMP_TAGINT, listall.data(), recvcounts.data(), displs.data(), MPI_LMP_TAGINT, world);

        // Add all unique IDs in listall to my map.
        for(int i = 0; i < nall; i++)
            idmap.insert(std::make_pair(listall[i], 0));
    } 
    else {
        comm->ring(n, sizeof(tagint), list.data(), 1, idring, NULL, (void *)&idmap, 0);
    }
    if(debugOutput) {
        MPI_Barrier(world);
        printLog("Gathered all cluster IDs: %i\n", idmap.size());
    }
    list.clear();
    int nclusters = 1;
    for(pos = idmap.begin(); pos != idmap.end(); ++pos) {
        pos->second = nclusters++;
    }
    if(debugOutput) {
        printLog("Number of clusters: %i\n", nclusters);
    }

    // Make sure all orientations have a positive determinant.
    for(int i = 0; i < atom->nlocal; i++) {
        if(local_orientations[i].atomCount != 0) {
            Matrix3& orient = local_orientations[i].orient;
            if(orient.determinant() < 0) {
                // Apply a reflection to make determinant positive.
                std::swap(orient[0][0], orient[0][1]);
                std::swap(orient[1][0], orient[1][1]);
                std::swap(orient[2][0], orient[2][1]);
            }
        }
    }

    // Count atoms in each cluster and compute mean lattice orientations, locally on each processor.
    std::vector<ClusterInfo> cluster_info(nclusters);
    memset(cluster_info.data(), 0, sizeof(ClusterInfo) * nclusters);
    for(int i = 0; i < atom->nlocal; i++) {
        tagint id = (tagint)local_orientations[i].atomCount;
        if(id != 0) {
            int clusterId = idmap[id];
            local_orientations[i].atomCount = clusterId;
            combine_orientations(cluster_info[clusterId].atomCount, cluster_info[clusterId].orient, 
                1, local_orientations[i].orient);
        }
    }
    if(debugOutput) {
        MPI_Barrier(world);
        printLog("Count cluster atoms\n");
    }

    // Send cluster orientations from all procs and combine them.
    std::vector<ClusterInfo> master_cluster_info(nclusters);
    MPI_Allreduce(cluster_info.data(), master_cluster_info.data(), nclusters, cluster_info_data_type, orientation_reduction_op, world);

    if(debugOutput) {
        MPI_Barrier(world);
        printLog("Finding largest cluster\n");
    }

    // Determine which cluster is the largest one.
    ClusterInfo* largestCluster = &master_cluster_info[0];
    for(int i = 1; i < nclusters; i++) {
        if(master_cluster_info[i].atomCount > largestCluster->atomCount) {
            largestCluster = &master_cluster_info[i];
        }
    }
    if(debugOutput) {
        MPI_Barrier(world);
        printLog("Performing polar decomposition\n");
    }

    // Perform polar decomposition F=R*U
    Matrix3 R, U;
    polar_decomposition_3x3(&largestCluster->orient[0][0], false, &R[0][0], &U[0][0]);

    // Map rotation to fundamental zone.
    map_to_fundamental_zone(R);

    // Output crystallite orientation.
    memcpy(&vector[0], &R, sizeof(double)*9);

    // Fraction of atoms in the largest cluster:
    vector[9] = largestCluster->atomCount / atom->natoms;

#if 0
        for(int i = 0; i < nclusters; i++) {
            const Matrix3& orient = master_cluster_info[i].orient;
            printLog("Cluster %i: %g atoms det=%f\n", i, master_cluster_info[i].atomCount, matrix_det(orient));
            for(int j = 0; j < 3; j++) {
                printLog("  %f %f %f  -  %f\n", orient[j][0], orient[j][1], orient[j][2], orient[j].length());
            }
        }
#endif
}

/*********************************************************************
 * Computes the mean of two crystal orientations weighted by the cluster size.
 *********************************************************************/
void CrystalOrientationCompute::combine_orientations(double& atomCountA, Matrix3& orientA, const double atomCountB, const Matrix3& orientB)
{
    // Find out which symmetry operation needs to be applied to make both orientations compatible.
    Matrix3 transformedOrient;
    double minDeviation = std::numeric_limits<double>::max();
    size_t bestSymmetryElement = 0;
    for(size_t s = 0; s < symmetryElements.size(); s++) {
        matrix_multiply(orientB, symmetryElements[s], transformedOrient);
        double deviation = 0;
        for(int i = 0; i < 3; i++)
            for(int j = 0; j < 3; j++)
                deviation += (transformedOrient[i][j] - orientA[i][j]) * (transformedOrient[i][j] - orientA[i][j]);
        if(deviation < minDeviation) {
            bestSymmetryElement = s;
            minDeviation = deviation;
        }
    }

    // Compute mean deformation gradient of the two clusters.
    matrix_multiply(orientB, symmetryElements[bestSymmetryElement], transformedOrient);
    for(int i = 0; i < 3; i++)
        for(int j = 0; j < 3; j++)
            orientA[i][j] = (atomCountA * orientA[i][j] + atomCountB * transformedOrient[i][j]) / (atomCountA + atomCountB);

    // Merge the sizes of the clusters.
    atomCountA += atomCountB;
}

void CrystalOrientationCompute::mpi_combine_orientations(void *invec, void *inoutvec, int *len, MPI_Datatype *datatype)
{
    ClusterInfo* clusterA = reinterpret_cast<ClusterInfo*>(invec);
    ClusterInfo* clusterB = reinterpret_cast<ClusterInfo*>(inoutvec);
    for(int i = 0; i < *len; i++, ++clusterA, ++clusterB) {
        if(clusterB->atomCount != 0 && clusterA->atomCount != 0) {
            combine_orientations(clusterB->atomCount, clusterB->orient, clusterA->atomCount, clusterA->orient);
        }
        else if(clusterA->atomCount != 0) {
            *clusterB = *clusterA;
        }
    }
}

void CrystalOrientationCompute::map_to_fundamental_zone(Matrix3& orientation)
{
    Matrix3 result;
    double minOmega = std::numeric_limits<double>::max();
    for(size_t s = 0; s < symmetryElements.size(); s++) {
        Matrix3 transformedOrient;
        matrix_multiply(orientation, symmetryElements[s], transformedOrient);
        // Compute the Rodrigues vector of the orientation matrix
        double cw = 0.5 * (transformedOrient.trace() - 1);
        double omega = acos(cw);
        if(omega < minOmega) {
            result = transformedOrient;
            minOmega = omega;
        }
    }
    orientation = result;
}


/* ----------------------------------------------------------------------
   callback from comm->ring()
   cbuf = list of N chunk IDs from another proc
   loop over the list, add each to my hash
   hash ends up storing all unique IDs across all procs
------------------------------------------------------------------------- */
void CrystalOrientationCompute::idring(int n, char *cbuf, void *ptr)
{
    tagint *list = (tagint *)cbuf;
    std::map<tagint,int>* idmap = (std::map<tagint,int>*)ptr;
    for(int i = 0; i < n; i++) 
        idmap->insert(std::make_pair(list[i], 0));
}

/*********************************************************************
 * This is for MPI communication with neighbor nodes.
 *********************************************************************/
int CrystalOrientationCompute::pack_forward_comm(int n, int* list, double* buf, int pbc_flag, int* pbc)
{
    int i,j,m;

    m = 0;
    if(commflag == CLUSTER) {
        for(i = 0; i < n; i++) {
            j = list[i];
            buf[m++] = local_orientations[j].atomCount;
        }
    } 
    return m;
}

/*********************************************************************
 * This is for MPI communication with neighbor nodes.
 *********************************************************************/
void CrystalOrientationCompute::unpack_forward_comm(int n, int first, double* buf)
{
    int i,m,last;

    m = 0;
    last = first + n;
    if(commflag == CLUSTER) {
        for (i = first; i < last; i++, m++) {
            if(local_orientations[i].atomCount == 0 || buf[m] < local_orientations[i].atomCount)
                local_orientations[i].atomCount = buf[m];
        }
    }
}

/*********************************************************************
 * Reports the memory usage of this fix to LAMMPS.
 *********************************************************************/
double CrystalOrientationCompute::memory_usage()
{
    double bytes = local_orientations.size() * sizeof(ClusterInfo);
    return bytes;
}

/*********************************************************************
 * Sends a formatted string to the log file and stdout.
 *********************************************************************/
void CrystalOrientationCompute::printLog(const char* format, ...) const
{
    if(comm->me == 0 && screen) {
        va_list ap;
        va_start(ap,format);
        fprintf(screen, "CrystalOrientationCompute: ");
        vfprintf(screen, format, ap);
        va_end(ap);
    }
    if(comm->me == 0 && logfile) {
        va_list ap;
        va_start(ap,format);
        fprintf(logfile, "CrystalOrientationCompute: ");
        vfprintf(logfile, format, ap);
        va_end(ap);
    }
}

/* ----------------------------------------------------------------------
  Find all atoms that are nearest neighbors of the given pair of atoms.
------------------------------------------------------------------------- */
static int findCommonNeighbors(const CrystalOrientationCompute::NeighborBondArray& neighborArray, int neighborIndex, unsigned int& commonNeighbors, int numNeighbors)
{
    commonNeighbors = neighborArray.neighborArray[neighborIndex];

#ifdef __GNUC__
    // Count the number of bits set in neighbor bit-field.
    return __builtin_popcount(commonNeighbors);
#else
    // Count the number of bits set in neighbor bit-field.
    unsigned int v = commonNeighbors - ((commonNeighbors >> 1) & 0x55555555);
    v = (v & 0x33333333) + ((v >> 2) & 0x33333333);
    return (((v + (v >> 4)) & 0xF0F0F0F) * 0x1010101) >> 24;
#endif
}

/* ----------------------------------------------------------------------
   Finds all bonds between common nearest neighbors.
------------------------------------------------------------------------- */
static int findNeighborBonds(const CrystalOrientationCompute::NeighborBondArray& neighborArray, unsigned int commonNeighbors, int numNeighbors, CNAPairBond* neighborBonds)
{
    int numBonds = 0;

    unsigned int nib[CrystalOrientationCompute::NeighborBondArray::CNA_MAX_PATTERN_NEIGHBORS];
    int nibn = 0;
    unsigned int ni1b = 1;
    for(int ni1 = 0; ni1 < numNeighbors; ni1++, ni1b <<= 1) {
        if(commonNeighbors & ni1b) {
            unsigned int b = commonNeighbors & neighborArray.neighborArray[ni1];
            for(int n = 0; n < nibn; n++) {
                if(b & nib[n])
                    neighborBonds[numBonds++] = ni1b | nib[n];
            }

            nib[nibn++] = ni1b;
        }
    }
    return numBonds;
}

/* ----------------------------------------------------------------------
   Find all chains of bonds.
------------------------------------------------------------------------- */
static int getAdjacentBonds(unsigned int atom, CNAPairBond* bondsToProcess, int& numBonds, unsigned int& atomsToProcess, unsigned int& atomsProcessed)
{
    int adjacentBonds = 0;
    for(int b = numBonds - 1; b >= 0; b--) {
        if(atom & *bondsToProcess) {
            ++adjacentBonds;
            atomsToProcess |= *bondsToProcess & (~atomsProcessed);
            memmove(bondsToProcess, bondsToProcess + 1, sizeof(CNAPairBond) * b);
            numBonds--;
        }
        else ++bondsToProcess;
    }
    return adjacentBonds;
}

/* ----------------------------------------------------------------------
   Find all chains of bonds between common neighbors and determine the length
   of the longest continuous chain.
------------------------------------------------------------------------- */
static int calcMaxChainLength(CNAPairBond* neighborBonds, int numBonds)
{
    // Group the common bonds into clusters.
    int maxChainLength = 0;
    while(numBonds) {
        // Make a new cluster, starting with the first remaining bond to be processed.
        numBonds--;
        unsigned int atomsToProcess = neighborBonds[numBonds];
        unsigned int atomsProcessed = 0;
        int clusterSize = 1;
        do {
            // Determine the number of trailing 0-bits in atomsToProcess,
            // starting at the least significant bit position.

#ifdef __GNUC__
            int nextAtomIndex = __builtin_ctz(atomsToProcess);
#else
            // Algorithm is from Bit Twiddling Hacks website.
            static const int MultiplyDeBruijnBitPosition[32] =
            {
              0, 1, 28, 2, 29, 14, 24, 3, 30, 22, 20, 15, 25, 17, 4, 8,
              31, 27, 13, 23, 21, 19, 16, 7, 26, 12, 18, 6, 11, 5, 10, 9
            };
            int nextAtomIndex = MultiplyDeBruijnBitPosition[((unsigned int)((atomsToProcess & -atomsToProcess) * 0x077CB531U)) >> 27];
#endif
            unsigned int nextAtom = 1 << nextAtomIndex;
            atomsProcessed |= nextAtom;
            atomsToProcess &= ~nextAtom;
            clusterSize += getAdjacentBonds(nextAtom, neighborBonds, numBonds, atomsToProcess, atomsProcessed);
        }
        while(atomsToProcess);
        if(clusterSize > maxChainLength)
            maxChainLength = clusterSize;
    }
    return maxChainLength;
}

/*******************************************************************************
 *  -/_|:|_|_\- 
 *
 *  This code is a modification of Theobald's QCP rotation code.
 *  It has been adapted to calculate the polar decomposition of a 3x3 matrix
 *  Adaption by PM Larsen
 *
 *  Original Author(s):      Douglas L. Theobald
 *                  Department of Biochemistry
 *                  MS 009
 *                  Brandeis University
 *                  415 South St
 *                  Waltham, MA  02453
 *                  USA
 *
 *                  dtheobald@brandeis.edu
 *                  
 *                  Pu Liu
 *                  Johnson & Johnson Pharmaceutical Research and Development, L.L.C.
 *                  665 Stockton Drive
 *                  Exton, PA  19341
 *                  USA
 *
 *                  pliu24@its.jnj.com
 * 
 *
 *    If you use this QCP rotation calculation method in a publication, please
 *    reference:
 *
 *      Douglas L. Theobald (2005)
 *      "Rapid calculation of RMSD using a quaternion-based characteristic
 *      polynomial."
 *      Acta Crystallographica A 61(4):478-480.
 *
 *      Pu Liu, Dmitris K. Agrafiotis, and Douglas L. Theobald (2009)
 *      "Fast determination of the optimal rotational matrix for macromolecular 
 *      superpositions."
 *      Journal of Computational Chemistry 31(7):1561-1563.
 *
 *
 *  Copyright (c) 2009-2013 Pu Liu and Douglas L. Theobald
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted
 *  provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice, this list of
 *    conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice, this list
 *    of conditions and the following disclaimer in the documentation and/or other materials
 *    provided with the distribution.
 *  * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific prior written
 *    permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 *  PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 *
 *  Source:         started anew.
 *
 *  Change History:
 *    2009/04/13      Started source
 *    2010/03/28      Modified FastCalcRMSDAndRotation() to handle tiny qsqr
 *                    If trying all rows of the adjoint still gives too small
 *                    qsqr, then just return identity matrix. (DLT)
 *    2010/06/30      Fixed prob in assigning A[9] = 0 in InnerProduct()
 *                    invalid mem access
 *    2011/02/21      Made CenterCoords use weights
 *    2011/05/02      Finally changed CenterCoords declaration in qcprot.h
 *                    Also changed some functions to static
 *    2011/07/08      put in fabs() to fix taking sqrt of small neg numbers, fp error
 *    2012/07/26      minor changes to comments and main.c, more info (v.1.4)
 *
 *      2016/05/29        QCP method adapted for polar decomposition of a 3x3 matrix.  For use in Polyhedral Template Matching.
 *  
 ******************************************************************************/
static void matmul(double* A, double* x, double* b)
{
    b[0] = A[0] * x[0] + A[1] * x[3] + A[2] * x[6];
    b[3] = A[3] * x[0] + A[4] * x[3] + A[5] * x[6];
    b[6] = A[6] * x[0] + A[7] * x[3] + A[8] * x[6];

    b[1] = A[0] * x[1] + A[1] * x[4] + A[2] * x[7];
    b[4] = A[3] * x[1] + A[4] * x[4] + A[5] * x[7];
    b[7] = A[6] * x[1] + A[7] * x[4] + A[8] * x[7];

    b[2] = A[0] * x[2] + A[1] * x[5] + A[2] * x[8];
    b[5] = A[3] * x[2] + A[4] * x[5] + A[5] * x[8];
    b[8] = A[6] * x[2] + A[7] * x[5] + A[8] * x[8];
}

static void quaternion_to_rotation_matrix(double* q, double* u)
{
    double a = q[0];
    double b = q[1];
    double c = q[2];
    double d = q[3];

    u[0] = a*a + b*b - c*c - d*d;
    u[1] = 2*b*c - 2*a*d;
    u[2] = 2*b*d + 2*a*c;

    u[3] = 2*b*c + 2*a*d;
    u[4] = a*a - b*b + c*c - d*d;
    u[5] = 2*c*d - 2*a*b;

    u[6] = 2*b*d - 2*a*c;
    u[7] = 2*c*d + 2*a*b;
    u[8] = a*a - b*b - c*c + d*d;
}

int CrystalOrientationCompute::polar_decomposition_3x3(double* _A, bool right_sided, double* U, double* P)
{
    const double evecprec = 1e-6;
    const double evalprec = 1e-11;

    double A[9] = {_A[0], _A[1], _A[2], _A[3], _A[4], _A[5], _A[6], _A[7], _A[8]};
    double det = A[0] * (A[4]*A[8] - A[5]*A[7]) - A[1] * (A[3]*A[8] - A[5]*A[6]) + A[2] * (A[3]*A[7] - A[4]*A[6]);
    if (det < 0)
    {
        for (int i=0;i<9;i++)
            A[i] = -A[i];
    }

    double    Sxx = A[0], Sxy = A[1], Sxz = A[2],
        Syx = A[3], Syy = A[4], Syz = A[5],
        Szx = A[6], Szy = A[7], Szz = A[8];

    double    Sxx2 = Sxx * Sxx, Syy2 = Syy * Syy, Szz2 = Szz * Szz,
        Sxy2 = Sxy * Sxy, Syz2 = Syz * Syz, Sxz2 = Sxz * Sxz,
        Syx2 = Syx * Syx, Szy2 = Szy * Szy, Szx2 = Szx * Szx;

    double SyzSzymSyySzz2 = 2.0*(Syz*Szy - Syy*Szz);
    double Sxx2Syy2Szz2Syz2Szy2 = Syy2 + Szz2 - Sxx2 + Syz2 + Szy2;
    double SxzpSzx = Sxz + Szx;
    double SyzpSzy = Syz + Szy;
    double SxypSyx = Sxy + Syx;
    double SyzmSzy = Syz - Szy;
    double SxzmSzx = Sxz - Szx;
    double SxymSyx = Sxy - Syx;
    double SxxpSyy = Sxx + Syy;
    double SxxmSyy = Sxx - Syy;
    double Sxy2Sxz2Syx2Szx2 = Sxy2 + Sxz2 - Syx2 - Szx2;

    double C[3];
    C[0] = Sxy2Sxz2Syx2Szx2 * Sxy2Sxz2Syx2Szx2
         + (Sxx2Syy2Szz2Syz2Szy2 + SyzSzymSyySzz2) * (Sxx2Syy2Szz2Syz2Szy2 - SyzSzymSyySzz2)
         + (-(SxzpSzx)*(SyzmSzy)+(SxymSyx)*(SxxmSyy-Szz)) * (-(SxzmSzx)*(SyzpSzy)+(SxymSyx)*(SxxmSyy+Szz))
         + (-(SxzpSzx)*(SyzpSzy)-(SxypSyx)*(SxxpSyy-Szz)) * (-(SxzmSzx)*(SyzmSzy)-(SxypSyx)*(SxxpSyy+Szz))
         + (+(SxypSyx)*(SyzpSzy)+(SxzpSzx)*(SxxmSyy+Szz)) * (-(SxymSyx)*(SyzmSzy)+(SxzpSzx)*(SxxpSyy+Szz))
         + (+(SxypSyx)*(SyzmSzy)+(SxzmSzx)*(SxxmSyy-Szz)) * (-(SxymSyx)*(SyzpSzy)+(SxzmSzx)*(SxxpSyy-Szz));

    C[1] = 8.0 * (Sxx*Syz*Szy + Syy*Szx*Sxz + Szz*Sxy*Syx - Sxx*Syy*Szz - Syz*Szx*Sxy - Szy*Syx*Sxz);

    C[2] = -2.0 * (Sxx2 + Syy2 + Szz2 + Sxy2 + Syx2 + Sxz2 + Szx2 + Syz2 + Szy2);

    double fnorm_squared = 0.0;
    for (int i=0;i<9;i++)
        fnorm_squared += A[i]*A[i];

    //Newton-Raphson
    double mxEigenV = sqrt(3 * fnorm_squared);
    if (mxEigenV > evalprec)
    {
        for (int i=0;i<50;i++)
        {
            double oldg = mxEigenV;
            double x2 = mxEigenV*mxEigenV;
            double b = (x2 + C[2])*mxEigenV;
            double a = b + C[1];
            double delta = ((a * mxEigenV + C[0]) / (2 * x2 * mxEigenV + b + a));
            mxEigenV -= delta;
            if (fabs(mxEigenV - oldg) < fabs(evalprec * mxEigenV))
                break;
        }
    }
    else
    {
        mxEigenV = 0.0;
    }

    double a11 = SxxpSyy + Szz - mxEigenV;
    double a12 = SyzmSzy;
    double a13 = -SxzmSzx;
    double a14 = SxymSyx;

    double a21 = SyzmSzy;
    double a22 = SxxmSyy - Szz  -mxEigenV;
    double a23 = SxypSyx;
    double a24 = SxzpSzx;

    double a31 = a13;
    double a32 = a23;
    double a33 = Syy - Sxx - Szz - mxEigenV;
    double a34 = SyzpSzy;

    double a41 = a14;
    double a42 = a24;
    double a43 = a34;
    double a44 = Szz - SxxpSyy - mxEigenV;

    double a3344_4334 = a33 * a44 - a43 * a34;
    double a3244_4234 = a32 * a44 - a42 * a34;
    double a3243_4233 = a32 * a43 - a42 * a33;
    double a3143_4133 = a31 * a43 - a41 * a33;
    double a3144_4134 = a31 * a44 - a41 * a34;
    double a3142_4132 = a31 * a42 - a41 * a32;

    double q1 =  a22*a3344_4334-a23*a3244_4234+a24*a3243_4233;
    double q2 = -a21*a3344_4334+a23*a3144_4134-a24*a3143_4133;
    double q3 =  a21*a3244_4234-a22*a3144_4134+a24*a3142_4132;
    double q4 = -a21*a3243_4233+a22*a3143_4133-a23*a3142_4132;

    double qsqr = q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4;
    double q[4];

    bool too_small = false;
    //The following code tries to calculate another column in the adjoint matrix when the norm of the 
    //current column is too small.
    //Usually this block will never be activated.  To be absolutely safe this should be
    //uncommented, but it is most likely unnecessary.
    if (qsqr < evecprec)
    {
        q1 =  a12*a3344_4334 - a13*a3244_4234 + a14*a3243_4233;
        q2 = -a11*a3344_4334 + a13*a3144_4134 - a14*a3143_4133;
        q3 =  a11*a3244_4234 - a12*a3144_4134 + a14*a3142_4132;
        q4 = -a11*a3243_4233 + a12*a3143_4133 - a13*a3142_4132;
        qsqr = q1*q1 + q2*q2 + q3*q3 + q4*q4;

        if (qsqr < evecprec)
        {
            double a1324_1423 = a13 * a24 - a14 * a23, a1224_1422 = a12 * a24 - a14 * a22;
            double a1223_1322 = a12 * a23 - a13 * a22, a1124_1421 = a11 * a24 - a14 * a21;
            double a1123_1321 = a11 * a23 - a13 * a21, a1122_1221 = a11 * a22 - a12 * a21;

            q1 =  a42 * a1324_1423 - a43 * a1224_1422 + a44 * a1223_1322;
            q2 = -a41 * a1324_1423 + a43 * a1124_1421 - a44 * a1123_1321;
            q3 =  a41 * a1224_1422 - a42 * a1124_1421 + a44 * a1122_1221;
            q4 = -a41 * a1223_1322 + a42 * a1123_1321 - a43 * a1122_1221;
            qsqr = q1*q1 + q2*q2 + q3*q3 + q4*q4;

            if (qsqr < evecprec)
            {
                q1 =  a32 * a1324_1423 - a33 * a1224_1422 + a34 * a1223_1322;
                q2 = -a31 * a1324_1423 + a33 * a1124_1421 - a34 * a1123_1321;
                q3 =  a31 * a1224_1422 - a32 * a1124_1421 + a34 * a1122_1221;
                q4 = -a31 * a1223_1322 + a32 * a1123_1321 - a33 * a1122_1221;
                qsqr = q1*q1 + q2*q2 + q3*q3 + q4*q4;
                
                if (qsqr < evecprec)
                {
                    //if qsqr is still too small, return the identity matrix.
                    q[0] = 1.0;
                    q[1] = 0.0;
                    q[2] = 0.0;
                    q[3] = 0.0;
                    U[0] = U[4] = U[8] = 1.0;
                    U[1] = U[2] = U[3] = U[5] = U[6] = U[7] = 0.0;
                    too_small = true;
                }
            }
        }
    }

    if (!too_small)
    {
        double normq = sqrt(qsqr);
        q1 /= normq;
        q2 /= normq;
        q3 /= normq;
        q4 /= normq;
        q[0] = -q1;
        q[1] = q2;
        q[2] = q3;
        q[3] = q4;
        quaternion_to_rotation_matrix(q, U);
    }

    if (det < 0)
    {
        for (int i=0;i<9;i++)
            U[i] = -U[i];
    }

    double invU[9] = {U[0], U[3], U[6], U[1], U[4], U[7], U[2], U[5], U[8]};

    if (right_sided)
        matmul(invU, _A, P);
    else
        matmul(_A, invU, P);

    return !too_small;
}
